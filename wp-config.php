<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '```mnJ1W3KWcsP)uBTVy1LR,04[Gas_)~7*0,gK;Z[iz~mT`i-yo*M6~kg&$sH%@' );
define( 'SECURE_AUTH_KEY',  '}2Ij=Sd@ D&sS!r>I0&P5|=_6m9)[{2ec7sGNL9L?|58]:3U[5CI]_r59h4*%fdj' );
define( 'LOGGED_IN_KEY',    'j<i<f;!F[.o>|{}We5Iz]n%?2nzeRdh&,C=I#:kUF*+%9eaxBX,cX|X2%>DM=C~F' );
define( 'NONCE_KEY',        'Wm?s]+o7)R!C:(;a&v.pfVtaf[Caj%aj{d?y1eK W_&*-mwHOnp1X45_>55H;*.e' );
define( 'AUTH_SALT',        'Yy8;-aT9^b|BVZ.!NaWyD0aQ>@V/G5%2oI-E[pr.3W4*4)|L4Nw.y8fp%Ocx_K8~' );
define( 'SECURE_AUTH_SALT', 'lKcns!Pj7)a?/J*T+[40$[e|%*enbFsyNkBn!jX88;1UStJ+?gn 5_sD8=V%}T8d' );
define( 'LOGGED_IN_SALT',   'ea[gj_nV43IMZT8Re0d[ufLTor^(@I?iDf{RvYx(4^I}<P:UJ4DO5B5f]`;Av.o!' );
define( 'NONCE_SALT',       '8IExZ_kW)D]0)&X`VR(GyqCWYLz3&vakC3J{ymbJEc[Hl Aljf@bQIz:nZ[lp LK' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
